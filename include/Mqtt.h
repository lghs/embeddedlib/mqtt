

#pragma once


#include <Wifi.h>
#include <WiFi.h>
#include <Arduino.h>
#include <PubSubClient.h>
#include "MqttStatusHook.h"
#include "MqttCallback.h"

#define MQTT_TIME_BETWEEN_RECONNECT 3000

class Mqtt {
    Wifi &wifi;
    WiFiClient wifiClient;
    PubSubClient client;
    MqttStatusHook *hook;

    bool lastStatus = 0;
    unsigned long lastReconnection = 0;
    const char *server;
    const char *id;
    const char *user;
    const char *password;
    MqttCallback* cb = 0;
    std::function<void(String, byte *, int)> callback = [&](String s, byte *b, int n) {
        if (this->cb) {
            cb->mqttCallback(s, b, n, this);
        }
    };

    void (*subscribe)(PubSubClient *pubSubClient);

    void reconnect();

public:
    Mqtt(Wifi &wifi);

    void setup(const char *server, const char *user, const char *password, const char *id, int port = 1883);

    void run();

    /**
     * hook when connected so the client can subscribe to desired channels
     * @param subscribe
     */
    void setSubscribe(void(*subscribe)(PubSubClient *pubSubClient));

    /**
     * callback when message received on topic
     * @param callback
     */
    void setCallback(MqttCallback* callback);

    void setStatusHook(MqttStatusHook *feedback);

    void publish(const char *string, const char *string1);

};



