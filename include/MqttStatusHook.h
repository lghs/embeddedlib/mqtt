

#pragma once


class MqttStatusHook {
public:
    virtual void connected() {};

    virtual void disconnected() {};

    virtual void connectionFailed() {};
};



