

#include "Mqtt.h"


Mqtt::Mqtt(Wifi &wifi) : wifi(wifi), wifiClient(), client(wifiClient) {

}

void Mqtt::setup(const char *server, const char *user, const char *password, const char *id, int port) {
    client.setServer(server, port);
    this->id = id;
    this->user = user;
    this->password = password;
}


void Mqtt::setCallback(MqttCallback * mqttCallback){
    this->cb = mqttCallback;
    client.setCallback(callback);
}

void Mqtt::reconnect() {
    unsigned long currentTime = millis();
    if (wifi.isConnected() && !client.connected()
        && (lastReconnection == 0
            || (currentTime - lastReconnection) > MQTT_TIME_BETWEEN_RECONNECT)
            ) {
        lastReconnection = millis();
        if (client.connect(id, user, password)) {
            Serial.println("mqtt connected");
            if (hook) {
                hook->connected();
            }
            lastStatus = 1;
            if (subscribe) {
                subscribe(&client);
            }
        } else {
            Serial.println("mqtt connection failed");
            if (hook) {
                hook->connectionFailed();
            }
        }

    }
}

void Mqtt::run() {

    if (!client.connected()) {
        Serial.println("mqtt disconnected");
        if (lastStatus && hook) {
            hook->disconnected();
        }
        lastStatus = 0;
        reconnect();
    } else {
        client.loop();
    }
}

void Mqtt::setSubscribe(void (*subscribe)(PubSubClient *)) {
    this->subscribe = subscribe;
}

void Mqtt::setStatusHook(MqttStatusHook *feedback) {
    this->hook = feedback;
}

void Mqtt::publish(const char *topic, const char *message) {
    client.publish(topic, message);
}
